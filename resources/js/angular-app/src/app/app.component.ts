import { Component, ɵConsole } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-app';

  fileData: File = null;
  originalImagePreviewUrl: any = null;
  newImagePreviewUrl: any = null;

  constructor(private http: HttpClient) {}
  ngOnInit() {}

  originalImagePreview() {
    // Show preview
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.originalImagePreviewUrl = reader.result;
    }
}

newImagePreview(imageData: Blob) {
  // Show preview
  var mimeType = imageData.type;
  if (mimeType.match(/image\/*/) == null) {
    return;
  }

  var reader = new FileReader();
  reader.readAsDataURL(imageData);
  reader.onload = (_event) => {
    this.newImagePreviewUrl = reader.result;
    // console.log(this.newImagePreviewUrl);
  }
}

  fileInputEvent(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
    this.originalImagePreview();
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('image', this.fileData); //$scope?
    this.http.post('http://presentr-test.test/upload', formData, { responseType: 'blob' })
      .subscribe(res => {
        console.log(res);
        alert("Upload Successful.");
        this.newImagePreview(res);
      })
  }
}
