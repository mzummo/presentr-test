Since we are using a client application (SPA) approach; the application is basically two parts (front-end and back-end). I put the application code in the laravel resources folder because thats where I normally build out react/vue apps and compile them to the public folder with webpack (though this step hasnt been done, it is being delivered as is in dev mode, without dist/prod and webpack compilation) and npm build functionality.

Due to time contraints on my end I took the approach of doing image manipulation in this case a simple greyscale filter. (while I could have done something more complex the setup time would have been longer than I would have liked due to the research needed to setup an Machine Learning and Model system locally or research a free alternative that I could use easly via an API). If requested I can accomplish this.

1) Setup a base laravel install with composer
2) Installed Angular via npm
	- Serving Laravel API with laragon and Angular with ng serve cmd.
3) Wrote a simple controller to handle a file upload and pass it to the Spatie Glide library to convert image to grayscale and assigned it to the route /upload (todo: mimetype checking, error handling)
4) I created a component called home and assigned it to the base route '/' within Angular rotuing
5) I modified the 'home' components html to create a form (importing Angular@FormModules) that showed a file upload form with a preview box for original and new image
6) On fileInput change event being firing the original image is displayed in a preview box
7) On formSubmit an Ajax request is done to the server where a FormData object is created and the Input File is attached/appended and submitted to the server (and expects a blob data type returned from the server). The Blob data returned is then passed to the preview function and attached to the src attribute after being properly processed/converted to a DataURI.
(error handling, unit tests, css are some things i could do to improve the code; as well as add file-upload progress or use a drag and drop feature)
As well as Angular Services and other more complex Angular concepts.