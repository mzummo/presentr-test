<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use GlideImage;

class HomeController extends Controller
{
    public function Index(Request $request) {
        //$request->file('file')->store('file');
    }

    public function Upload(Request $request) {

        if (!$request->hasFile('image')) { // todo: filetype check
            $returnData = array(
                'status' => 'error',
                'message' => 'Missing Image'
            );
            return Response::json($returnData, 400);
        }

        $path = $request->file('image')->store('images'); // ommit store to get tmp dir path instead
        GlideImage::create(storage_path('app/').$path)
            ->modify(['filt'=>'greyscale'])
            ->save(storage_path('app/public/') . 'newImage.png');

        return response()->file(storage_path('app/public/') . 'newImage.png');

        // return [
        //     'fullPath' => storage_path('app/').$path,
        //     'uploadPath' => $path,
        //     'publicStoragePath' => storage_path('app/public'),
        //     'info' => pathinfo($path)
        // ];
        // return $request->file('image');
        // return $path;
    }
}
